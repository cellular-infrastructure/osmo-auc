/* (C) 2012 by Harald Welte <laforge@gnumonks.org>
 *
 * All Rights Reserved
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include <stdlib.h>
#include <sys/time.h>

#include <osmocom/core/talloc.h>
#include <osmocom/crypt/auth.h>

#include "auc.h"

void *tall_ctx;

static int auc_test(void)
{
	int rc, i, success = 0;
	char imsi[15+1];
	struct osmo_auth_vector vecs[3];
	struct timeval start, end, res;
	uint64_t usec;

	gettimeofday(&start, NULL);
	for (i = 0; i < 100000; i++) {
		snprintf(imsi, 16, "90170%010u", i);
		rc = auc_gen_vecs(vecs, imsi, 3);
		if (rc < 0)
			fprintf(stderr, "IMSI %s: error %d\n", imsi, rc);
		success++;
	}
	gettimeofday(&end, NULL);
	timersub(&end, &start, &res);

	usec = res.tv_sec * 1000000 + res.tv_usec;

	printf("Generated 3 vectors for each of %u IMSIs in %u usec\n",
		success, usec);
	printf("=> %f requests of 3vec / sec\n", success / ((float) usec / 1000000));

	return 0;
}

int main(int argc, char **argv)
{
	int rc;

	tall_ctx = talloc_zero_size(NULL, 1);

	rc = auc_core_init(tall_ctx, 64*1024);
	if (rc < 0)
		exit(1);

	rc = auc_rand_init();
	if (rc < 0)
		exit(1);

	rc = auc_storage_read(tall_ctx, "auc.txt");
	printf("%u records read from disk\n", rc);

	auc_test();

	exit(0);
}
