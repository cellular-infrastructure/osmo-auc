#!/usr/bin/perl -w

# this script can generate a CSV file with the specified number of
# entries, which in turn can be used from osmo-auc.

use strict;

my $COUNT = $ARGV[0];
my $i;

for ($i = 0; $i < $COUNT; $i++) {
	printf("90170%010u,5,%032u,%032u,0000,%08u,0\n", $i, $i, $i, $i);
}
