#ifndef _AUC_INT_H
#define _AUC_INT_H

/* (C) 2012 by Harald Welte <laforge@gnumonks.org>
 *
 * All Rights Reserved
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include <osmocom/core/linuxlist.h>
#include <osmocom/crypt/auth.h>

struct auc_rec {
	struct llist_head list;
	struct llist_head hash_list;
	uint8_t imsi[15+1];
	long offset_sqn;
	struct osmo_sub_auth_data auth;
};

int auc_core_init(void *ctx, unsigned int nbuckets);
int auc_add_rec(struct auc_rec *rec);
int auc_gen_vecs(struct osmo_auth_vector *vec,
		 const char *imsi, int n_vecs);
int auc_gen_vec_auts(struct osmo_auth_vector *vec,
		     const char *imsi, const uint8_t *rand_auts,
		     const uint8_t *auts);

/* auc_rand.c */
int auc_rand_init(void);
int auc_get_rand(uint8_t *rand, unsigned int len);

#endif

